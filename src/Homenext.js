import  React from 'react';
import './Homenext.css';
import logo from './images/logo.png';
import reactjs from './images/reactjs.jpg';
const Homenext = () =>{
    return(
        <div className="total">
        <div className="nav">
            <div className="logo"><img src={logo}></img>
            </div>
            <div className="dropdown">
                <ul>
                    <div className='dropdown1'><li><a href="home.html">All Courses</a></li></div>
                    <li><a href="#">My Dashboard</a></li>
                </ul>
            </div>
            <div className="logout">
                <button className="logbutton"><a href="index.html">Logout</a></button>
            </div>
        </div>
        <div className="head">
            <h1>React js Basics</h1>
        </div>
        <div className='instructors'>
            <p><b />Instructors : <b /> John Doe,Steve Rogers</p>
            <p>Duration : 19 hours</p>
            </div>
        <div className='date'>
            <p>Course Starts : 19-12-2020</p>
            <p>Course Ends : 15-06-2021</p>
        </div>
        <div className='pg'>
            <p>React makes it painless to create interactive UIs.Design simple views for each</p>
            <p>state in your application, and React will efficiently update and render just the</p>
            <p>right components when your data changes.</p><br />
            <p>Declarative  views make your code more predictable and easier to debug. Build</p>
            <p>encapsulated components that manage their own state,then compose them to</p>
            <p>make complex UIs.</p><br />
            <p>Since component logic is written in Javascript instead of templates, you can</p>
            <p>easily pass rich data through your app and keep state out of the DOM.</p>
        </div>
        <div className='button'><button>Enroll Now</button></div>
        <div classname='image'><img src={reactjs}></img></div>
        </div>
    );
}
export default Homenext;