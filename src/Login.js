import React, { useState } from "react";
import './Login.css';
import Background from './images/Background.jpg';
import logo from './images/logo.png';
  
const Login = () =>{
     // React States
  const [errorMessages, setErrorMessages] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);

  // User Login info
  const database = [
    {
      username: "kanimurugan2277@gmail.com",
      password: "banking@07"
    },
  ];

  const errors = {
    uname: "invalid username",
    pass: "invalid password"
  };

  const handleSubmit = (event) => {
    //Prevent page reload
    event.preventDefault();

    var { uname, pass } = document.forms[0];

    // Find user login info
    const userData = database.find((user) => user.username === uname.value);

    // Compare user info
    if (userData) {
      if (userData.password !== pass.value) {
        // Invalid password
        setErrorMessages({ name: "pass", message: errors.pass });
      } else {
        setIsSubmitted(true);
      }
    } else {
      // Username not found
      setErrorMessages({ name: "uname", message: errors.uname });
    }
  };

  // Generate JSX code for error message
  const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

  // JSX code for login form
  const renderForm = (
    <div className="form">
      <form onSubmit={handleSubmit}>
        <div className="input-container">
          <label>Customer ID</label>
          <input type="text" name="uname" required />
          {renderErrorMessage("uname")}
        </div>
        <div className="input-container">
          <label>Password </label>
          <input type="password" name="pass" required />
          {renderErrorMessage("pass")}
        </div>
        <div className="button-container">
            <button type="submit">Login</button>
        </div>
      </form>
    </div>
  );
return(
    <div className='all'>
        <div className='background'><img src={Background}></img>
        <div className='logo'><img src={logo}></img></div>
        <div className='header'>
        <div className='heading1'><h1>Welcome to</h1></div>
        <div className='heading2'><h1>Crystal Delta</h1></div>
        <div className='heading3'><h1>E-Banking</h1></div>
        <div className='form-box'><div className="app">
      <div className="login-form">
        <div className="title">Login to your account</div>
        {isSubmitted ? <div>User is successfully logged in</div> : renderForm}
      </div>
    </div></div>
        </div></div>
    </div>
)
}
export default Login;