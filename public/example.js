import React from 'react'
import {card} from 'semantic-ui-react'

const cardExampleFluid =() => (
    <card.Group>
        <card fluid color='red' header='option 1'/>
        <card fluid color='orange' header='option 2'/>
        <card fluid color='yellow' header='option 3'/>
    </card.Group>
)
export default cardExampleFluid    